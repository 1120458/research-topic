# Continuous Integration

## O que é Continuous Integration?
Continuous integration, ou CI, consiste na prática de integrar o código dos diversos desenvolvedores e fazer builds periodicas. O conceito começou há mais de 20 anos e o seu principal objetivo era evitar os muitos problemas que a integração de código pode trazer quando é feita apenas no final do projeto. Para além de resolver esses problemas, também poupa aos programadores tempo e dinheiro, uma vez que é bastante mais fácil encontrar e resolver pequenos problemas assim que eles surgem do que deixar que estes se acumulem e resolvê-los no fim.

Inicialmente os programadores tinham a responsabilidade de integrar o código que desenvolviam, mas agora as ferramentas de CI permitem que esta integração seja feita de forma automática. Estas ferramentas podem ser configuradas para executar builds com determinada periodicidade ou assim que entra novo código no repositório. Quando isto acontece, são executados  automaticamente scripts de teste e verificam que tudo se comporta como esperado. Após estes testes passarem com sucesso há o deploy das builds para servidores de testes e ficam disponíveis para serem testadas manualmente e posteriormente incorporarem uma release.

Os servidores de Continuous integration podem ser configurados para integrar a cada 10 minutos, uma vez por dia ou cada vez que o programador interage com o servidor. Podem ainda fazer testes quando existem merge requests de forma a verificar se existem problemas com o novo código introduzido e nesse caso abortar o merge e gerar um relatório de erros. Este processo permite às equipas imprevistos temporais nos momentos de entrega dos projetos uma vez que os problemas vão sendo resolvidos iterativamente. O grande objetivo da CI é então garantir que o código no servidor não contém problemas e se mantém estável a cada instante.
Cada equipa de desenvolvimento tem necessidades diferentes, o que significa que não existe uma ferramenta de CI ideal. É por isso que existem dezenas de ferramentas disponíveis para que se possa escolher a que melhor se adapta às necessidades. Por exemplo, a melhor ferramenta de CI para projetos open source pode não ser a ideal para software empresarial. Dois destes casos vão ser discutidos nesta apresentação, o Jenkins e o Travis CI, que são indicados para diferentes tipos de projeto.

## Automated testing
Testes automatizados são testes que podem ser executados sem a necessidade de intervenção humana, a qualquer momento. Normalmente é necessário escrever um script para validar o comportamento do seu aplicativo. O script é então executado por uma máquina que fornece os resultados como um output. O teste automatizado é uma parte essencial do CI.

## Continuous Delivery
É uma abordagem de engenharia de software na qual as equipas produzem software em ciclos curtos, garantindo que este possa ser lançado de forma confiável a qualquer momento. Destina-se a construir, testar e lançar software com maior velocidade e frequência. Esta abordagem ajuda a reduzir o custo, o tempo e o risco de mudanças, permitindo atualizações mais incrementais.

## Continuous Deployment
A implantação contínua consiste num processo de automatização entre alterações no código e disponibilização desse mesmo código para o cliente.
Segundo esta abordagem, após qualquer alteração de código, caso os testes de CI tenham sucesso, este é enviado para produção sem a necessidade de interação humana. Isto geralmente resulta num aumento das implantações diárias e consequentemente fornece feedback mais frequente e rápido à equipa de desenvolvimento.

# Describe the approach CI with TravisCI

## O que é TravisCI?
TravisCI é uma plataforma de integração contínua que auxilia o processo de desenvolvimento, uma vez que faz build e testa automaticamente as alterações de código, fornecendo imediatamente feedback das mudanças ocorridas. Por outro lado, o TravisCI também pode automatizar outras partes do processo de desenvolvimento pela gestão de implantações e notificações.

## Como funciona o TravisCI?
Aquando da execução de uma build, a plataforma TravisCI clona determinado repositório do GitHub para um novo ambiente virtual e efetua as tarefas necessárias de modo a fazer build e a testar o código.
Caso nenhuma das tarefas falhe, a build passa e o TravisCI pode implantar o código para um web server ou um host de aplicações. Por outro lado, na hipótese de uma ou mais tarefas falharem a build não é considerada bem sucedida.
Na integração contínua através do uso do TravisCI há a possibilidade de configurar de modo a ter tarefas dependentes umas das outras.

## Definições importantes no TravisCI
Um job é considerado um processo automatizado que clona um repositório para um ambiente virtual e posteriormente realiza uma sucessão de phases, nomeadamente compilação de código e execução de testes. Caso um job falhe é retornado um código diferente de zero na script phase.
Uma phase corresponde aos passos sequenciais de um job, que podem seguir a seguinte ordem: install phase, script phase e deploy phase.
Build é um grupo de jobs que termina quando todos esses jobs acabam. Por exemplo, uma build pode ser composta por dois jobs e os mesmos podem possuir uma versão diferente da mesma linguagem de programação.
Stage corresponde a um grupo de jobs que correm em paralelo e que fazem parte de um processo de build sequencial composto por várias etapas.

## Alguns casos de interrupção da build no TravisCI
A build é considerada interrompida no caso de um ou mais jobs finalizarem com um estado que não corresponda a sucesso.
No caso de errored o job para imediatamente. Perante este comando é retornado um output diferente de zero numa das seguintes phases: before_install, install ou before_script.
Failed equivale a um comando que na script phase retornou um output diferente de zero e o job é executado até à sua conclusão.
Canceled acontece quando um utilizador cancela um job antes do mesmo terminar.


# Case study with TravisCI

## Caso de estudo : Solução EMF com TravisCI

Como já explicado, o TravisCI é uma ferramenta que permite a automatização de processos de "build", "testing" e "deployment", entre outros. Todos estes processos são comumente organizados numa estrutura apelidada de "pipeline". No caso específico de uma solução Eclipse Modeling Framework (EMF). São necessários alguns requisitos para que a pipeline funcione de forma correta. Algumas das dependências do projecto EMF, como demonstrado nas aulas leccionadas em EDOM, são geridas e resolvidas através de um pom.xml file que é usado pelo Maven.

O Maven permite desta forma a resolução de alguns "lifecycles", como "mvn clean", "mvn compile", "mvn install", entre outros. Assim estes lifecycles podem ser executados de forma automatizada a seguir a cada "push" para o repositório, ao invés de obrigar o "developer" a manualmente correr sempre estes comandos, garantindo assim que tudo se encontra estável e pronto a ser "deployed" num outro ambiente, seja este de desenvolvimento, testes ou produção.

## Como construir pipelines em TravisCI

O TravisCI funciona através de ficheiros do tipo YAML sendo que a extensão do ficheiro segue o seguinte padrão .travis.yml, onde são definidos todos os Jobs, Stages e Scripts a serem executados. Também neste ficheiro são definidas como devem ser executadas as Stages, isto é de forma sequencial ou paralelamente. Este ficheiro tem que estar na raiz do repositório para que a "pipeline" corra de forma correta. É importante denotar que os ambientes "host" em TravisCI são todos baseados em Unix, o que faz com que os comandos a serem definidos neste ficheiro também tenham que ser compatíveis com este tipo de sistema operativo.

Exemplo de um ficheiro .travis.yml que permite a execução de simples comandos Maven:

```yaml
language: java

script: mvn test

jdk:
- oraclejdk9
- oraclejdk8
- openjdk8
```

A flag "language" identifica ao ambiente host de TravisCI algumas das dependências obrigatórias que irá instalar, neste caso o "java" irá criar um ambiente com Gradle, Maven e Ant instalados. Sendo que caso seja detectado um pom.xml, característico de um project que usa Maven como gestor de dependências, antes de correr a build, o TravisCI irá instalar todas as dependências com recurso a este comando:

`mvn install -DskipTests=true -Dmaven.javadoc.skip=true -B -V`

Como demonstrado na configuração acima, uma das grandes vantagens do TravisCI é que permite a integração de vários tipos de Java Development Kits (JDK), o que permite garantir com maior segurança a robustez da solução visto que os comandos definidos correram em diferentes ambientes, permitindo a deteção de erros de compatibilidade de forma eficaz.


## Referências

* https://docs.travis-ci.com/user/languages/java/



# Integração Contínua em EMF

## Desenvolvimento vs Utilização de EMF
Nas aulas de EDOM foram abordados alguns conceitos de modelação a partir de representação de metamodelos e representação textual. Foram ainda abordadas as técnicas de transformação de modelos e ainda geração de código a partir modelos ferramentas de modelação.
Como mecanismo de automação de dependências foi usado o Maven, isso permite que as ferramentas sejam executadas e utilizadas num ambiente exterior ao Eclipse. 

## Maven and Travis CI
Para pequenos projetos, o Maven consegue identificar dependencias entre aplicações e tratá-las facilmente, no entanto, em projetos maiores, existe um crescente número de ficheiros XML, o que dificulta a perceção do projeto e consequentemente a sua manutenção.
O Maven permite reutilizar código e recursos sendo compatível com uma vasta gama de IDEs de Java e permite, também, transferir os recursos a partir de repositórios na rede/internet. No entanto os seus erros de compilação podem ser bastante extensos e de difícil entendimento.
Algumas das vantagens do Travis CI são a fácil integração com a conta do GitHub, o facto de possuir um ambiente de teste fácil de configurar, recebendo notificações rápidas e úteis quando a compilação falha e suporta bastantes linguagens de programação e tecnologias (ANDROID, C, C#, C++, CLOJURE, CRYSTAL, D, DART, ELIXIR, ELM, ERLANG, F#, GO, GROOVY, HASKELL, HAXE, JAVA, JAVASCRIPT (WITH NODE.JS), JULIA, NIX, OBJECTIVE-C, PERL, PERL6, PHP, PYTHON, R, RUBY, RUST, SCALA, SMALLTALK, SWIFT, VISUAL BASIC). 
No caso dos pequenos projetos desenvolvidos em EDOM, como foi usado o Maven para a gestão de dependências seria fácil a utilização de TravisCI para a aplicação de integração contínua, visto que este é suportado em várias versões na ferramenta.
Bastaria executar na pipeline os comandos de "mvn install" para instalação de dependências e as aplicações estariam em condições de serem executadas e testadas no ambiente de CI.

## Referência
* https://docs.travis-ci.com/user/languages/java/#projects-using-maven