﻿<?xml version="1.0" encoding="utf-8"?>
<Dsl xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="0d6cf154-4ba3-4c7f-bc6c-e3f6e478e452" Description="Description for Isep.Mindmap.Mindmap" Name="Mindmap" DisplayName="Mindmap" Namespace="Isep.Mindmap" ProductName="Mindmap" CompanyName="Isep" PackageGuid="f6396ed6-bf3b-401c-8892-a6fedfa86669" PackageNamespace="Isep.Mindmap" xmlns="http://schemas.microsoft.com/VisualStudio/2005/DslTools/DslDefinitionModel">
  <Classes>
    <DomainClass Id="10b5bf1b-340e-4968-8bd0-57411224d0e2" Description="The root in which all other elements are embedded. Appears as a diagram." Name="Map" DisplayName="Map" Namespace="Isep.Mindmap">
      <Properties>
        <DomainProperty Id="0e85b381-0898-4786-a825-c974ce84336e" Description="Description for Isep.Mindmap.Map.Title" Name="Title" DisplayName="Title">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Notes>Creates an embedding link when an element is dropped onto a model. </Notes>
          <Index>
            <DomainClassMoniker Name="MapElement" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>MapHasMapElements.MapElements</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="1ee43326-5e4b-4817-9c63-f3a6971c4282" Description="Elements embedded in the model. Appear as boxes on the diagram." Name="MapElement" DisplayName="Map Element" Namespace="Isep.Mindmap">
      <Properties>
        <DomainProperty Id="6eef86a6-907f-4e5a-853f-b42c47db2ba8" Description="Description for Isep.Mindmap.MapElement.Name" Name="Name" DisplayName="Name" DefaultValue="" IsElementName="true">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
    <DomainClass Id="14763ad6-bff1-490f-96b8-e4ed3effb4a2" Description="Description for Isep.Mindmap.Relationship" Name="Relationship" DisplayName="Relationship" Namespace="Isep.Mindmap">
      <BaseClass>
        <DomainClassMoniker Name="MapElement" />
      </BaseClass>
      <Properties>
        <DomainProperty Id="97f00e5d-adff-45e6-82f0-51fa5d8bbe79" Description="Description for Isep.Mindmap.Relationship.Type" Name="type" DisplayName="Type">
          <Type>
            <DomainEnumerationMoniker Name="Type" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
    <DomainClass Id="85f139c7-ed40-4814-8bf6-4a429e89640c" Description="Description for Isep.Mindmap.Topic" Name="Topic" DisplayName="Topic" Namespace="Isep.Mindmap">
      <BaseClass>
        <DomainClassMoniker Name="MapElement" />
      </BaseClass>
      <Properties>
        <DomainProperty Id="db541837-964f-44e8-b221-33911bf7a10e" Description="Description for Isep.Mindmap.Topic.Description" Name="Description" DisplayName="Description">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="a65dc836-28b9-4086-ac10-5caebd6f37f3" Description="Description for Isep.Mindmap.Topic.Start" Name="start" DisplayName="Start">
          <Type>
            <ExternalTypeMoniker Name="/System/DateTime" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="f5e67cb4-1159-4e29-bc5a-8bf8ffb9cea2" Description="Description for Isep.Mindmap.Topic.End" Name="end" DisplayName="End">
          <Type>
            <ExternalTypeMoniker Name="/System/DateTime" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="54c48781-f4ca-4420-a33d-3bbfb21f8c50" Description="Description for Isep.Mindmap.Topic.Priority" Name="priority" DisplayName="Priority">
          <Type>
            <DomainEnumerationMoniker Name="Priority" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
  </Classes>
  <Relationships>
    <DomainRelationship Id="e1fb0c6a-a2b0-4adc-b64b-14626948185b" Description="Embedding relationship between the Model and Elements" Name="MapHasMapElements" DisplayName="Map Has Map Elements" Namespace="Isep.Mindmap" IsEmbedding="true">
      <Source>
        <DomainRole Id="172d7b3d-1ff2-4163-9e3c-1e63bab7afe8" Description="" Name="Map" DisplayName="Map" PropertyName="MapElements" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Map Elements">
          <RolePlayer>
            <DomainClassMoniker Name="Map" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="f6e03483-60c3-4c16-91fd-71998e6c90a3" Description="" Name="Element" DisplayName="Element" PropertyName="Element_Map" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Element_ Map">
          <RolePlayer>
            <DomainClassMoniker Name="MapElement" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="8a8081e3-8ea5-43e8-aad7-28bb522180fe" Description="Reference relationship between Elements." Name="MapElementReferencesTargets" DisplayName="Map Element References Targets" Namespace="Isep.Mindmap">
      <Source>
        <DomainRole Id="72a52ecc-24b2-4228-8d12-59c0786d7c32" Description="Description for Isep.Mindmap.ExampleRelationship.Target" Name="Source" DisplayName="Source" PropertyName="Targets" PropertyDisplayName="Targets">
          <RolePlayer>
            <DomainClassMoniker Name="MapElement" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="c2ffb587-8ac9-436a-b166-a69c5af2992c" Description="Description for Isep.Mindmap.ExampleRelationship.Source" Name="Target" DisplayName="Target" PropertyName="Sources" PropertyDisplayName="Sources">
          <RolePlayer>
            <DomainClassMoniker Name="MapElement" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="04c34722-3f97-4a05-83bb-ea1c7b23b68f" Description="Description for Isep.Mindmap.TopicReferencesTopics" Name="TopicReferencesTopics" DisplayName="Topic References Topics" Namespace="Isep.Mindmap">
      <Source>
        <DomainRole Id="25f21f51-40f4-49af-ba12-1fd3402eb08b" Description="Description for Isep.Mindmap.TopicReferencesTopics.SourceTopic" Name="SourceTopic" DisplayName="Source Topic" PropertyName="subtopics" PropertyDisplayName="Subtopics">
          <RolePlayer>
            <DomainClassMoniker Name="Topic" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="520d256a-5f25-400f-b6a2-8cf8c33d82a4" Description="Description for Isep.Mindmap.TopicReferencesTopics.TargetTopic" Name="TargetTopic" DisplayName="Target Topic" PropertyName="parent" Multiplicity="ZeroOne" PropertyDisplayName="Parent">
          <RolePlayer>
            <DomainClassMoniker Name="Topic" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
  </Relationships>
  <Types>
    <ExternalType Name="DateTime" Namespace="System" />
    <ExternalType Name="String" Namespace="System" />
    <ExternalType Name="Int16" Namespace="System" />
    <ExternalType Name="Int32" Namespace="System" />
    <ExternalType Name="Int64" Namespace="System" />
    <ExternalType Name="UInt16" Namespace="System" />
    <ExternalType Name="UInt32" Namespace="System" />
    <ExternalType Name="UInt64" Namespace="System" />
    <ExternalType Name="SByte" Namespace="System" />
    <ExternalType Name="Byte" Namespace="System" />
    <ExternalType Name="Double" Namespace="System" />
    <ExternalType Name="Single" Namespace="System" />
    <ExternalType Name="Guid" Namespace="System" />
    <ExternalType Name="Boolean" Namespace="System" />
    <ExternalType Name="Char" Namespace="System" />
    <DomainEnumeration Name="Priority" Namespace="Isep.Mindmap" IsFlags="true" Description="Description for Isep.Mindmap.Priority">
      <Literals>
        <EnumerationLiteral Description="Description for Isep.Mindmap.Priority.HIGH" Name="HIGH" Value="0" />
        <EnumerationLiteral Description="Description for Isep.Mindmap.Priority.MEDIUM" Name="MEDIUM" Value="1" />
        <EnumerationLiteral Description="Description for Isep.Mindmap.Priority.LOW" Name="LOW" Value="2" />
      </Literals>
    </DomainEnumeration>
    <DomainEnumeration Name="Type" Namespace="Isep.Mindmap" IsFlags="true" Description="Description for Isep.Mindmap.Type">
      <Literals>
        <EnumerationLiteral Description="Description for Isep.Mindmap.Type.DEPENDENCY" Name="DEPENDENCY" Value="0" />
        <EnumerationLiteral Description="Description for Isep.Mindmap.Type.INCLUDE" Name="INCLUDE" Value="1" />
        <EnumerationLiteral Description="Description for Isep.Mindmap.Type.EXTEND" Name="EXTEND" Value="2" />
      </Literals>
    </DomainEnumeration>
  </Types>
  <Shapes>
    <GeometryShape Id="13353462-ca20-4059-8407-ec11d2bdbbe7" Description="Shape used to represent ExampleElements on a Diagram." Name="MapShape" DisplayName="Map Shape" Namespace="Isep.Mindmap" FixedTooltipText="Map Shape" FillColor="242, 239, 229" OutlineColor="113, 111, 110" InitialWidth="2" InitialHeight="0.75" OutlineThickness="0.01" Geometry="Rectangle">
      <Notes>The shape has a text decorator used to display the Name property of the mapped ExampleElement.</Notes>
      <ShapeHasDecorators Position="Center" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="NameDecorator" DisplayName="Name Decorator" DefaultText="NameDecorator" />
      </ShapeHasDecorators>
    </GeometryShape>
    <GeometryShape Id="8a516957-0ba4-485c-a2c5-35e09617a2a5" Description="Description for Isep.Mindmap.TopicShape" Name="TopicShape" DisplayName="Topic Shape" Namespace="Isep.Mindmap" FixedTooltipText="Topic Shape" InitialWidth="2" InitialHeight="0.75" OutlineThickness="0.01" Geometry="Rectangle">
      <ShapeHasDecorators Position="Center" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="DescriptionDecorator" DisplayName="Description Decorator" DefaultText="DescriptionDecorator" />
      </ShapeHasDecorators>
    </GeometryShape>
  </Shapes>
  <Connectors>
    <Connector Id="1363aa7e-593d-4052-9473-19da76f03035" Description="Connector between the ExampleShapes. Represents ExampleRelationships on the Diagram." Name="MapElementConnector" DisplayName="Map Element Connector" Namespace="Isep.Mindmap" FixedTooltipText="Map Element Connector" Color="113, 111, 110" TargetEndStyle="EmptyArrow" Thickness="0.01" />
    <Connector Id="43a9f810-f09e-498c-a685-a7f15e12087f" Description="Description for Isep.Mindmap.TopicConnector" Name="TopicConnector" DisplayName="Topic Connector" Namespace="Isep.Mindmap" FixedTooltipText="Topic Connector" />
  </Connectors>
  <XmlSerializationBehavior Name="MindmapSerializationBehavior" Namespace="Isep.Mindmap">
    <ClassData>
      <XmlClassData TypeName="Map" MonikerAttributeName="" SerializeId="true" MonikerElementName="mapMoniker" ElementName="map" MonikerTypeName="MapMoniker">
        <DomainClassMoniker Name="Map" />
        <ElementData>
          <XmlRelationshipData RoleElementName="mapElements">
            <DomainRelationshipMoniker Name="MapHasMapElements" />
          </XmlRelationshipData>
          <XmlPropertyData XmlName="title">
            <DomainPropertyMoniker Name="Map/Title" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="MapElement" MonikerAttributeName="name" SerializeId="true" MonikerElementName="mapElementMoniker" ElementName="mapElement" MonikerTypeName="MapElementMoniker">
        <DomainClassMoniker Name="MapElement" />
        <ElementData>
          <XmlPropertyData XmlName="name" IsMonikerKey="true">
            <DomainPropertyMoniker Name="MapElement/Name" />
          </XmlPropertyData>
          <XmlRelationshipData RoleElementName="targets">
            <DomainRelationshipMoniker Name="MapElementReferencesTargets" />
          </XmlRelationshipData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="MapHasMapElements" MonikerAttributeName="" SerializeId="true" MonikerElementName="mapHasMapElementsMoniker" ElementName="mapHasMapElements" MonikerTypeName="MapHasMapElementsMoniker">
        <DomainRelationshipMoniker Name="MapHasMapElements" />
      </XmlClassData>
      <XmlClassData TypeName="MapElementReferencesTargets" MonikerAttributeName="" SerializeId="true" MonikerElementName="mapElementReferencesTargetsMoniker" ElementName="mapElementReferencesTargets" MonikerTypeName="MapElementReferencesTargetsMoniker">
        <DomainRelationshipMoniker Name="MapElementReferencesTargets" />
      </XmlClassData>
      <XmlClassData TypeName="MapShape" MonikerAttributeName="" SerializeId="true" MonikerElementName="mapShapeMoniker" ElementName="mapShape" MonikerTypeName="MapShapeMoniker">
        <GeometryShapeMoniker Name="MapShape" />
      </XmlClassData>
      <XmlClassData TypeName="MapElementConnector" MonikerAttributeName="" SerializeId="true" MonikerElementName="mapElementConnectorMoniker" ElementName="mapElementConnector" MonikerTypeName="MapElementConnectorMoniker">
        <ConnectorMoniker Name="MapElementConnector" />
      </XmlClassData>
      <XmlClassData TypeName="MindmapDiagram" MonikerAttributeName="" SerializeId="true" MonikerElementName="mindmapDiagramMoniker" ElementName="mindmapDiagram" MonikerTypeName="MindmapDiagramMoniker">
        <DiagramMoniker Name="MindmapDiagram" />
      </XmlClassData>
      <XmlClassData TypeName="Relationship" MonikerAttributeName="" SerializeId="true" MonikerElementName="relationshipMoniker" ElementName="relationship" MonikerTypeName="RelationshipMoniker">
        <DomainClassMoniker Name="Relationship" />
        <ElementData>
          <XmlPropertyData XmlName="type">
            <DomainPropertyMoniker Name="Relationship/type" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="Topic" MonikerAttributeName="" SerializeId="true" MonikerElementName="topicMoniker" ElementName="topic" MonikerTypeName="TopicMoniker">
        <DomainClassMoniker Name="Topic" />
        <ElementData>
          <XmlPropertyData XmlName="description">
            <DomainPropertyMoniker Name="Topic/Description" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="start">
            <DomainPropertyMoniker Name="Topic/start" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="end">
            <DomainPropertyMoniker Name="Topic/end" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="priority">
            <DomainPropertyMoniker Name="Topic/priority" />
          </XmlPropertyData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="subtopics">
            <DomainRelationshipMoniker Name="TopicReferencesTopics" />
          </XmlRelationshipData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="TopicShape" MonikerAttributeName="" SerializeId="true" MonikerElementName="topicShapeMoniker" ElementName="topicShape" MonikerTypeName="TopicShapeMoniker">
        <GeometryShapeMoniker Name="TopicShape" />
      </XmlClassData>
      <XmlClassData TypeName="TopicConnector" MonikerAttributeName="" SerializeId="true" MonikerElementName="topicConnectorMoniker" ElementName="topicConnector" MonikerTypeName="TopicConnectorMoniker">
        <ConnectorMoniker Name="TopicConnector" />
      </XmlClassData>
      <XmlClassData TypeName="TopicReferencesTopics" MonikerAttributeName="" SerializeId="true" MonikerElementName="topicReferencesTopicsMoniker" ElementName="topicReferencesTopics" MonikerTypeName="TopicReferencesTopicsMoniker">
        <DomainRelationshipMoniker Name="TopicReferencesTopics" />
      </XmlClassData>
    </ClassData>
  </XmlSerializationBehavior>
  <ExplorerBehavior Name="MindmapExplorer" />
  <ConnectionBuilders>
    <ConnectionBuilder Name="MapElementReferencesTargetsBuilder">
      <Notes>Provides for the creation of an ExampleRelationship by pointing at two ExampleElements.</Notes>
      <LinkConnectDirective>
        <DomainRelationshipMoniker Name="MapElementReferencesTargets" />
        <SourceDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="MapElement" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </SourceDirectives>
        <TargetDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="MapElement" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </TargetDirectives>
      </LinkConnectDirective>
    </ConnectionBuilder>
    <ConnectionBuilder Name="TopicReferencesTopicsBuilder">
      <LinkConnectDirective>
        <DomainRelationshipMoniker Name="TopicReferencesTopics" />
        <SourceDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="Topic" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </SourceDirectives>
        <TargetDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="Topic" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </TargetDirectives>
      </LinkConnectDirective>
    </ConnectionBuilder>
  </ConnectionBuilders>
  <Diagram Id="427ab09b-79a8-4c90-aa04-945a172ba5b6" Description="Description for Isep.Mindmap.MindmapDiagram" Name="MindmapDiagram" DisplayName="Minimal Language Diagram" Namespace="Isep.Mindmap">
    <Class>
      <DomainClassMoniker Name="Map" />
    </Class>
    <ShapeMaps>
      <ShapeMap>
        <DomainClassMoniker Name="MapElement" />
        <ParentElementPath>
          <DomainPath>MapHasMapElements.Element_Map/!Map</DomainPath>
        </ParentElementPath>
        <DecoratorMap>
          <TextDecoratorMoniker Name="MapShape/NameDecorator" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="MapElement/Name" />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <GeometryShapeMoniker Name="MapShape" />
      </ShapeMap>
      <ShapeMap>
        <DomainClassMoniker Name="Topic" />
        <ParentElementPath>
          <DomainPath>MapHasMapElements.Element_Map/!Map</DomainPath>
        </ParentElementPath>
        <DecoratorMap>
          <TextDecoratorMoniker Name="TopicShape/DescriptionDecorator" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="Topic/Description" />
              <DomainPath>TopicReferencesTopics.parent/!SourceTopic</DomainPath>
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <GeometryShapeMoniker Name="TopicShape" />
      </ShapeMap>
    </ShapeMaps>
    <ConnectorMaps>
      <ConnectorMap>
        <ConnectorMoniker Name="MapElementConnector" />
        <DomainRelationshipMoniker Name="MapElementReferencesTargets" />
      </ConnectorMap>
      <ConnectorMap>
        <ConnectorMoniker Name="TopicConnector" />
        <DomainRelationshipMoniker Name="TopicReferencesTopics" />
      </ConnectorMap>
    </ConnectorMaps>
  </Diagram>
  <Designer CopyPasteGeneration="CopyPasteOnly" FileExtension="mindmap" EditorGuid="945be8e2-6bf4-41d3-9070-bf3ff548a881">
    <RootClass>
      <DomainClassMoniker Name="Map" />
    </RootClass>
    <XmlSerializationDefinition CustomPostLoad="false">
      <XmlSerializationBehaviorMoniker Name="MindmapSerializationBehavior" />
    </XmlSerializationDefinition>
    <ToolboxTab TabText="Mindmap">
      <ElementTool Name="MapElement" ToolboxIcon="resources\exampleshapetoolbitmap.bmp" Caption="MapElement" Tooltip="Create an MapElement" HelpKeyword="CreateExampleClassF1Keyword">
        <DomainClassMoniker Name="MapElement" />
      </ElementTool>
      <ConnectionTool Name="MapRelationship" ToolboxIcon="resources\exampleconnectortoolbitmap.bmp" Caption="MapRelationship" Tooltip="Drag between MapElements to create an MapRelationship" HelpKeyword="ConnectExampleRelationF1Keyword">
        <ConnectionBuilderMoniker Name="Mindmap/MapElementReferencesTargetsBuilder" />
      </ConnectionTool>
      <ElementTool Name="Topic" ToolboxIcon="Resources\ExampleShapeToolBitmap.bmp" Caption="Topic" Tooltip="Create an Topic" HelpKeyword="Topic">
        <DomainClassMoniker Name="Topic" />
      </ElementTool>
      <ConnectionTool Name="TopicRelantionship" ToolboxIcon="Resources\ExampleConnectorToolBitmap.bmp" Caption="TopicRelantionship" Tooltip="Topic Relantionship" HelpKeyword="TopicRelantionship">
        <ConnectionBuilderMoniker Name="Mindmap/TopicReferencesTopicsBuilder" />
      </ConnectionTool>
    </ToolboxTab>
    <Validation UsesMenu="false" UsesOpen="false" UsesSave="false" UsesLoad="false" />
    <DiagramMoniker Name="MindmapDiagram" />
  </Designer>
  <Explorer ExplorerGuid="e587a2f0-40bb-4964-97a0-667e4a5c5050" Title="Mindmap Explorer">
    <ExplorerBehaviorMoniker Name="Mindmap/MindmapExplorer" />
  </Explorer>
</Dsl>